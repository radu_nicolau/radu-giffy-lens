blipp = require('blippar').blipp;
var scene = blipp.addScene();

var sW = blipp.getScreenWidth() * 1.003;
var sH = blipp.getScreenHeight() * 1.003;
var sW0 = blipp.getScreenWidth();
var sH0 = blipp.getScreenHeight();
var PivotScaler = 0.01;
var FlashColors = ['ff0000', 'ffff00', 'ff00ff', '00ff00', '00ffff', '0000ff'];
var Rays = [];
var RaysOn = 0;
var RaysOnCoef = 0;
var RaysTotal = 35;
var GifsStarted = false;
var LastImage = 0;
var PhotoReady = true;

function startGifs() {
  if (!GifsStarted) {
    GifsStarted = true;
    blipp.vibrate();
    Glow.setHidden(true);
    Spikey.setHidden(true);
    Flash.setHidden(true);
    for (var i = 0; i < RaysTotal; i++) {
      Rays[i].setHidden(true);
    }

    Warp.setAlpha(1);
    Warp.animate().alpha(0).delay(500).duration(500);
    Warp.setActiveMesh(8).animate().activeMesh(30).duration(700);

    //  !!! Start the gif block here
  }
}

scene.onCreate = function () {

  screen = scene.getScreen();

  Warp = screen.addMesh('Warp.md2')
    .setActiveMesh(0)
    .setScale([sH0 / 1000, sH0 / 1000, 15])
    .setAlpha(0)
    .setTextureScale([sH0 / sW0, 1])
    .setTextureOffset([(1 - sH0 / sW0) / 2, 1])
    .setBlend('add')
    .setType('phantom');

  Origin = screen.addTransform();

  Origin.onUpdate = function () {
    if (scene.getTouchIds().length > 0) {
      if (RaysOn < RaysTotal) {
        RaysOn += RaysOnCoef;
      }
      if (RaysOn > 5 && !GifsStarted) {
        Rays[RaysOn - 1].setHidden(false);
      }
    }
  }

  Pivot = Origin.addTransform();
  Pivot.onUpdate = function () {
    var s = this.getScale()[0];
    var a = 0.1;
    this.setScale(s * (1 - a) + PivotScaler * a);
  }

  Glow = Pivot.addSprite(['Glow.png', 'Glow-A.jpg'])
    .setScale(4)
    .setColor('ffffff')
    .setType('phantom');

  for (var i = 0; i < RaysTotal; i++) {

    var Flashy = Origin.addMesh('Flashy.b3m')
      .setTexture('Gradient.png')
      .setBlend('add')
      .setHidden(true)
      .setType('phantom');

    Flashy.onDraw = function () {
      this.setScale([(Math.random() * 3 + 1) * sH / 5, sH / 5, 1])
      this.setRotationZ(Math.random() * 360)
      this.setColor(FlashColors[Math.floor(Math.random() * FlashColors.length)])
      return 1;
    }

    Rays.push(Flashy);
  }

  Spikey = Pivot.addMesh('Spikey.b3m')
    .setScale(1)
    .setColor('ffffff')
    .setType('solid');

  Spikey.onDraw = function () {
    this.setRotation([Math.random() * 360, Math.random() * 360, Math.random() * 360])
  }

  Flash = screen.addSprite()
    .setScale([sW, sH, 1])
    .setColor('ffffff00');

  Flash.onDraw = function () {
    var a = 2 * RaysOn / RaysTotal - 0.8;
    this.setAlpha(a);
    if (a >= 1) {
      startGifs();
    } else {
      GifsStarted = false;
    }
  }

  Sensor = screen.addSprite()
    .setScale([sW, sH, 1])
    .setColor('ffffff00');

  Sensor.onTouchStart = function (id, touchX, touchY) {
    Pivot.setScale(sH / 32);
    PivotScaler = sH / 6;
    Origin.setTranslation([touchX, touchY, 0])
    if (PhotoReady) {
      PhotoReady = false;
      LastImage++;
      Glow.setHidden(true);
      Spikey.setHidden(true);
      blipp.takePhoto({
        saveToAssets: true,
        saveToGallery: false,
        filename: 'CurrentScreen_' + LastImage + '.jpg',
        scale: 512
      }, function (result) {
        Glow.setHidden(false);
        Spikey.setHidden(false);
        Warp.setTexture(['CurrentScreen_' + LastImage + '.jpg', 'Warp-A.jpg']);
        RaysOnCoef = 1;
        PhotoReady = true;
      })
    }
  }

  Sensor.onTouchEnd = function (id, touchX, touchY) {
    for (var i = 0; i < Rays.length; i++) {
      Rays[i].setHidden(true);
      Rays[i].onDraw();
    }
    RaysOnCoef = 0;
    RaysOn = 0;
    PivotScaler = 0.01;

    Glow.setHidden(false);
    Spikey.setHidden(false);
    Flash.setHidden(false);
    Pivot.setScale(0.01);
  }

  ResetBlipp = scene.getScreen().addSprite().setHAlign('left').setVAlign('bottom').setScale(blipp.getScreenWidth() / 6).setColor('0000ff0f')
    .setTranslation([-blipp.getScreenWidth() / 2, -blipp.getScreenHeight() / 2, 20])

  ResetBlipp.onTouchEnd = function () {
    blipp.goToBlipp(blipp.getAddress());
  }
}

scene.onShow = function () {
  blipp.uiVisible('navBar', false);
}