blipp = require('blippar').blipp;
var scene = blipp.addScene();

scene.onCreate = function () {

  var TRN = scene.addMaterial('TRN')
    //.setType('reflective').setReflectionTextures(['View_0.jpg', 'View_1.jpg', 'View_2.jpg', 'View_3.jpg', 'View_4.jpg', 'View_5.jpg'])
    .setType('shiny')
    .setShininess(200)
    .setSpecularPower(1)
    .setTexture(['Texture.jpg', 'Alpha.png'])
    .setNormalTexture('Normals.jpg')
    .setSelfLightIntensity(0.2);
  // .setSpecularTexture('Bump.jpg');

  var s = 388 * 800 / 1024;

  Gif = scene.addSprite().setName('Gif')
    .setTexture('cat6.jpg')
    .setScale([s * 480 / 248, s, 1])
    .setType('solid');

  Gif.onDraw = function () {
    Gif.playVideo('cat6.mp4', '', true, false, false)
    return 1
  }

  Frame = scene.addMesh('quad.b3m')
    .setTranslationZ(1)
    .setScale(800)
    .setColor('f5bf4c')
    .setType('phantom')
    .setMaterial('TRN');

}

scene.onTouchMove = function (id, touchX, touchY) {
  blipp.goToBlipp(blipp.getAddress())
}